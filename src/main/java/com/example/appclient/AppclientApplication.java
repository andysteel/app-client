package com.example.appclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableAutoConfiguration
public class AppclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppclientApplication.class, args);
	}
	
	@Configuration
	public static class SecurityPermitAllConfig extends WebSecurityConfigurerAdapter {

//	    @Override
//	    protected void configure(HttpSecurity http) throws Exception {
//	        http.authorizeRequests().anyRequest().permitAll()  
//	            .and().csrf().disable();
//	    }
	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http.authorizeRequests()
	        	.antMatchers("/ola/**").permitAll()
	        	.antMatchers("/actuator/**","/instances").authenticated()
	            .and().httpBasic()
	            .and()
	            .csrf()
	            .ignoringAntMatchers("/actuator/**","/instances");
	    }
	}
}
